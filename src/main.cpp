#include <Arduino.h>
#include <LoRa.h>
#include <SPI.h>
#include <U8x8lib.h>

#define LORA_SCK  5
#define LORA_MISO 19
#define LORA_MOSI 27
#define LORA_CS   18
#define LORA_RST  14
#define LORA_IRQ  26
#define LORA_FREQ 433E6
// The larger the spreading factor the greater the range but slower data rate.
// Send and receive radios need to be set the same.
#define LORA_SPREAD_FACTOR 12 // ranges from 6-12, default 7 see API docs

#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16

#define LED 25

U8X8_SSD1306_128X64_NONAME_HW_I2C display(OLED_RST, OLED_SCL, OLED_SDA);

void init(void);

void setup(void)
{
  Serial.begin(9600);
  while (!Serial);

  Serial.println("Setup display");
  display.begin();

  Serial.println("Setup SPI");
  SPI.begin(LORA_SCK, LORA_MISO, LORA_MOSI, LORA_CS);

  Serial.println("Setup LoRa");
  LoRa.setPins(LORA_CS, LORA_RST, LORA_IRQ);

  pinMode(LED, OUTPUT);

  init();
}

static unsigned int receiveInProgress = 0;

void onMsgReceive(int packetSize)
{
  receiveInProgress = 1;
  Serial.print("Received packet '");

  String packet = "";
  for (int i = 0; i < packetSize; i++) {
    packet += (char) LoRa.read();
  }

  //display.setCursor(0, 0);
  //display.print("> " + packet + "\n");

  Serial.println(packet + "'");
  receiveInProgress = 0;
}

void init(void)
{
  Serial.println("Configure LoRa");
  if (!LoRa.begin(LORA_FREQ)) {
    Serial.println("Configuring LoRa failed!");
    while (1);
  }

  LoRa.setSpreadingFactor(LORA_SPREAD_FACTOR);

  // Change the transmit power of the radio
  // Default is LoRa.setTxPower(17, PA_OUTPUT_PA_BOOST_PIN);
  // Most modules have the PA output pin connected to PA_BOOST, gain 2-17
  // TTGO and some modules are connected to RFO_HF, gain 0-14
  // If your receiver RSSI is very weak and little affected by a better antenna, change this!
  LoRa.setTxPower(14, PA_OUTPUT_RFO_PIN);

  LoRa.onReceive(onMsgReceive);
  LoRa.receive();

  display.setFont(u8x8_font_chroma48medium8_r);
  display.setCursor(0, 0);
  display.print("Waiting for messages");
}

unsigned int counter = 0;

void loop(void)
{
  // The code here bellow transmits LoRa packets. For some reason, transmitting seems to break the receive IRQ.
  // Remove the following line to make the device transmit packets at the cost of not being able to recieve.
  return;

  while (receiveInProgress)
    delay(1);

  LoRa.beginPacket();
  LoRa.print("HeLoRa! ");
  LoRa.print(counter);
  LoRa.endPacket();
  LoRa.idle();
  LoRa.receive();

  Serial.println("Sent message");

  counter++;
  delay(10);
}
